import random

from scraping import AmazonBrowser, AmazonException


class AmazonBrowserWrapper(AmazonBrowser):

    def __init__(self, proxies=None):
        AmazonBrowser.__init__(self, proxies)

    @staticmethod
    def _search_string(asin, keyword):
        return "{0}, {1}".format(keyword, asin)

    def _find_similar_book_title(self):
        pass

    @staticmethod
    def _set_search_delay(delay):
        """
        Delay is received in seconds, we make it to ms and add a random factor to it
        :param delay:
        :return:
        """
        # delay_ms = delay * 1000
        # Make delay not constant

        # Add a ten percent delay
        new_delay = delay + random.randint(-2, 2)

        if new_delay <= 0:
            return delay
        return new_delay

    def run_search(self, book_info):

        if book_info.keywords:
            # For every new book - enter amazon again
            self._enter_homepage()
            for keyword, repetitions, delay in book_info.keywords:

                # Switch proxy every keyword
                self._set_proxies(random_proxy=True)

                for i in range(repetitions):

                    self.search(search_string=self._search_string(asin=book_info.asin,
                                                                  keyword=keyword))
                    # print self.response().read()
                    try:

                        # if not book_info.name:
                        # print book_info.show()

                        self.follow_link_by_name(match_phrase=book_info.name)
                        # print book_info.show()
                        # if not book_info.name:
                        #     print "missing book info %s", book_info.show()

                        print "success for %s with %s, %s" % (book_info.name, book_info.asin, keyword)
                        # print self.response().read()
                    except AmazonException as e:
                        print e, book_info.show()
                    # Wait for the requested time
                    delay = 10
                    delay_time = self._set_search_delay(delay)
                    # print "waiting %s seconds delay" % delay_time
                    # sleep(self._set_search_delay(delay_time))
        else:
            raise AmazonException("No keywords received")

