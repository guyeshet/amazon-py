import csv
from settings import BookInfo
from datatypes import convert_data_types


class CSVReader:

    def __init__(self, input_file):

        self.input_file = input_file

        self.book_info_list = []

    def parse_file(self):

        with open(self.input_file, 'rU') as csv_file:
            reader = csv.reader(csv_file)
            kw_list = []

            book_info = BookInfo(name="", asin="", keywords=kw_list)

            # Go over the lines in the file
            for row in reader:

                # First check if its the first row
                if self._check_if_book_title_row(row):

                    # First we want to check if we are not in the first time
                    # In the second round the list will already have values
                    if kw_list:
                        book_info.keywords = kw_list
                        # book_info.keywords = kw_list
                        self.book_info_list.append(book_info)
                        # Clear the keyword list upon a new book name
                        kw_list = []
                        # Create a new object
                        book_info = BookInfo(name="", asin="", keywords=kw_list)

                    # We always check if we are in the first row
                    book_name, asin = self._find_book_and_asin(row)
                    if book_name:
                        book_info.name = book_name
                        book_info.asin = asin
                else:
                    kw_list.append(self._parse_keyword_line(row))

        # Handle last row
        if kw_list:
            # book_info.keywords = kw_list
            self.book_info_list.append(book_info)

    @staticmethod
    def _parse_keyword_line(row):
        """
        This method returns a tuple with the (keyword, repeat_times)
        :param row:
        :return:
        """
        # 1: Keyword is in the first position
        # 2: number of repetitions
        # 3: Delay between repetitions

        return convert_data_types(row[1]), convert_data_types(row[2]), convert_data_types(row[3])

    @staticmethod
    def _check_if_book_title_row(row):
        """
        This function checks if its the first row in the file
        :param row:
        :return:
        """
        number = row[0]
        if number:
            if "#" in number:
                return True
        return False

    @staticmethod
    def _find_book_and_asin(row):
        """
        We try to find the first row in the CSV file
        :param row:
        :return:
        """
        # row[0] is the number
        book_name = row[1]
        asin = row[2]

        return book_name, asin

# input_file = r"S:\simplestory\amazon\books.orig.csv"
# joiner = CSVReader(input_file)
# joiner.parse_file()
