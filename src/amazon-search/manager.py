from csv_parser import CSVReader
from amazon_wrapper import AmazonBrowserWrapper
from settings import HTTP_PROXY_DETAILS, HTTP_PROXY

input_file = r"books.csv"

file_parser = CSVReader(input_file)

amazon_wrapper = AmazonBrowserWrapper(proxies={HTTP_PROXY: HTTP_PROXY_DETAILS})
# amazon_wrapper = AmazonBrowserWrapper()

# Parse the input file
file_parser.parse_file()

# Go over the list of books to search
for book_info in file_parser.book_info_list:
    amazon_wrapper.run_search(book_info)

# try:
#     # Go over the list of books to search
#     for book_info in file_parser.book_info_list:
#         amazon_wrapper.run_search(book_info)
#
# except Exception as e:
#     print "Exception in the manager main process %s" % e

