import json

import requests
from settings import ProxyDetails, HTTP_PROXY_DETAILS

API_URL = "http://gimmeproxy.com/api/getProxy"


def get_new_proxy_ip(api_url):

    response = requests.get(api_url)
    if response.ok:
        try:
            response_json = json.loads(response.text)
            # Return the proxy url
            return ProxyDetails(url=response_json["ip"],
                                port=response_json["port"]).to_url()

        except Exception as e:
            print "Error parsing JSON from %s: %s" % (api_url, e)
            # Return Default proxy on exception
            return HTTP_PROXY_DETAILS
