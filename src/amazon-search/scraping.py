import random
import re

from mechanize import Browser
from settings import USER_AGENT, HTTP_PROXY, HTTP_PROXY_DETAILS
from proxy_parser import get_new_proxy_ip, API_URL


def remove_special_chars(text):
    # This function removes all special chars from a string
    if text:
        return re.sub('[^A-Za-z0-9]+', '', text)
    return text


class AmazonException(Exception):

    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        super(AmazonException, self).__init__(message)


class AmazonBrowser(Browser):
    SEARCH_FORM_NAME = "site-search"
    MAIN_URL = "https://www.amazon.com/"

    def __init__(self, proxies=None):
        Browser.__init__(self)

        self.proxies = proxies

        # set the user agent value
        self.user_agents = self._load_user_agents("user_agents.txt")
        self._set_user_agent()
        # Removed Proxies until finding a new free proxy
        self._set_proxies()
        self._enter_homepage()

    def _enter_homepage(self):
        """
        Enter Amazon
        :return:
        """
        self.open(self.MAIN_URL)

    def _set_proxies(self, random_proxy=False):
        """
        This method sets an external proxy, or fetches a new one from a web API
        :param random_proxy:
        :return:
        """
        # Fetching a new proxy address is random is assigned
        if random_proxy:
            proxy_url = get_new_proxy_ip(API_URL)
            self.proxies = {HTTP_PROXY: proxy_url}

        self.set_proxies(self.proxies)

    def _set_user_agent(self):
        """
        This method selects a random user agent for each operation
        :return:
        """
        ua = random.choice(self.user_agents)  # select a random user agent
        # print ua
        self.addheaders = [(USER_AGENT, ua),
                           ("Connection", "close")]

    def _select_search_form(self, form_name="site-search"):
        """
        This method selects a form in BR by its name
        :param form_name:
        :return:
        """
        for form in self.forms():
            try:
                if form.attrs['name'] == form_name:
                    self.form = form
                    break
            except KeyError as e:
                print "Couldn't find for attribute %s" % e
                # print self.response().read()
            except ValueError as e:
                print "Other form error %s" % e

    def search(self, search_string):

        # Each search operation is started with a new user agent
        self._set_user_agent()

        # selects the relevant form on the website
        self._select_search_form(form_name=self.SEARCH_FORM_NAME)
        if self.form:
            # Searching for a key word
            self.form["field-keywords"] = search_string
            self.submit()
        else:
            print "Could'nt find form"
            print self.response().read()

    def follow_link_by_name(self, match_phrase):
        link_to_use = None
        # print match_phrase
        stripped_match_phrase = remove_special_chars(match_phrase)
        # for link in self.links(text_regex=match_phrase):
        for link in self.links():
            # We strip all special chars to avoid problem with quotes
            link_text = remove_special_chars(link.text)
            if link_text:
                # print stripped_match_phrase, link_text
                if stripped_match_phrase in link_text:
                    link_to_use = link
                    break
            # break
        if link_to_use:
            self.follow_link(link_to_use)  # link still holds the last value it had in the loop
        else:
            raise AmazonException("No Link Found for match phrase %s" % match_phrase)

    @staticmethod
    def _load_user_agents(uafile):
        """
        uafile : string
            path to text file of user agents, one per line
        """
        uas = []
        with open(uafile, 'rb') as uaf:
            for ua in uaf.readlines():
                if ua:
                    uas.append(ua.strip()[1:-1 - 1])
        random.shuffle(uas)
        return uas
#
# br = AmazonBrowser(proxies={HTTP_PROXY: HTTP_PROXY_DETAILS},
#                    )
#
# # print br.response().read()
# br.search(search_string="B009X1DMFW, investment for beginners")
# br.follow_link_by_name(match_phrase="Investing Guide")
# print br.response().read()
#
# br.search(search_string="B00CJQFRMY, money")
# try:
#     br.follow_link_by_name(match_phrase="millionaire")
#     print br.response().read()
# except AmazonException as e:
#     print e
#
