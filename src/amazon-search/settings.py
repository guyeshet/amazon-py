
class BookInfo:

    def __init__(self, name, asin, keywords):
        self.name = name
        self.asin = asin
        self.keywords = keywords

    def show(self):
        return "%s, %s, %s" % (self.name, self.asin, self.keywords)


class ProxyDetails:

    def __init__(self, url, port, user="", password="", ):
        self.user = user
        self.password = password
        self.url = url
        self.port = port

    def to_url(self):
        if self.user or self.password:
            return "{0}:{1}@{2}:{3}".format(self.user,
                                            self.password,
                                            self.url,
                                            self.port)
        else:
            return "{0}:{1}".format(self.url,
                                    self.port)

HTTP_PROXY = "http"
# Proxy setting
# HTTP_PROXY_DETAILS = ProxyDetails(user="guyeshet@gmail.com",
#                                   password="guyESHET90",
#                                   url="fr.proxymesh.com",
#                                   port=31280).to_url()
# print HTTP_PROXY_DETAILS

HTTP_PROXY_DETAILS = ProxyDetails("189.219.97.33",
                                  port=56570).to_url()

USER_AGENT = "User-Agent"
USER_AGENT_CONST = 'Mozilla/5.0 (Windows NT 10.0)'
